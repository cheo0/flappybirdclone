using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepetidorTuberias : MonoBehaviour
{
    public int numTuberias = 5;
    public GameObject tuberia;
    public float radioSpawn;
    public float minTuberia;
    public float maxTuberia;

    private GameObject[] tuberias;
    private Vector2 posicion = new Vector2(-15f, -25f);
    private float tiempoDesdeUltimo;
    private float posicionX = 10f;
    private int tuberiaActual = 0;

    // Start is called before the first frame update
    void Start()
    {
        tuberias = new GameObject[numTuberias];
        for (int i = 0; i < numTuberias; i++)
        {
            tuberias[i] = Instantiate(tuberia, posicion, Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        tiempoDesdeUltimo += Time.deltaTime;
        if (tiempoDesdeUltimo >= radioSpawn)
        {
            tiempoDesdeUltimo = 0;
            var posicionY = Random.Range(minTuberia, maxTuberia);
            tuberias[tuberiaActual].transform.position = new Vector2(posicionX, posicionY);
            tuberiaActual = tuberiaActual + 1 >= numTuberias ? 0 : tuberiaActual + 1;
            
        }
    }
}
