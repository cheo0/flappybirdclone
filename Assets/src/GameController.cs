using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController instance;
    public GameObject gameOverText;
    public Text scoreText;
    public Text maxScoreText;
    public bool gameOver;
    public int score;
    public bool gameStarted;

    private void Awake()
    {
        Time.timeScale = 0;
        gameStarted = false;
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (gameOver && (Input.GetMouseButtonDown(0) || Input.touches.Length > 0))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        else if (!gameOver && gameStarted)
        {
            scoreText.text = "Score: " + score.ToString();
        }
    }

    public void BirdDied()
    {
        var maxScore = PlayerPrefs.GetFloat("Max Score");
        if (maxScore < score)
        {
            PlayerPrefs.SetFloat("Max Score", score);
            maxScore = score;
        }
        maxScoreText.text = "Max Score: " + maxScore.ToString();
        gameOver = true;
        gameOverText.SetActive(true);
    }
}
