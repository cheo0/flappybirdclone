using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour {
    public Rigidbody2D rb;

    public float velocidadImpulso;

    public int puntaje;

    public bool isDead;

    private void Awake() {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update() {
        if (!isDead && (Input.GetMouseButtonDown(0) || Input.touches.Length > 0)) {
            if (!GameController.instance.gameStarted)
            {
                GameController.instance.gameStarted = true;
                Time.timeScale = 1;
            }
            rb.velocity = Vector2.up * velocidadImpulso;
        }
    }

    public void IncrementScore()
    {
        puntaje += 1;
        GameController.instance.score = puntaje;
    }

    public void BirdDied()
    {
        isDead = true;
        GameController.instance.BirdDied();
    }
}